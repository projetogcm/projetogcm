


$(document).ready(function () {
    var c = $("#mid-c")[0];
    var cs = $("#mid-c-sharp")[0];
    var d = $("#mid-d")[0];
    var ds = $("#mid-d-sharp")[0];
    var e = $("#mid-e")[0];
    var f = $("#mid-f")[0];
    var fs = $("#mid-f-sharp")[0];
    var g = $("#mid-g")[0];
    var gs = $("#mid-g-sharp")[0];
    var a = $("#mid-a")[0];
    var as = $("#mid-a-sharp")[0];
    var b = $("#mid-b")[0];

    //c.play();
    $("li").click(function () {
        var note = this.dataset.note;
        console.log($('#txtarea').val())
        //alert(note);
        switch (note) {
            case "a": 
                a.play();
                $('#txtarea').val($('#txtarea').val() + 'A;');
                break;
            case "b": 
                b.play();
                $('#txtarea').val($('#txtarea').val() + 'B;');
                break;
            case "c": 
                c.play();
                $('#txtarea').val($('#txtarea').val() + 'C;');
                break;
            case "d": 
                d.play();
                $('#txtarea').val($('#txtarea').val() + 'D;');
                break;

            case "e": 
                e.play();
                $('#txtarea').val($('#txtarea').val() + 'E;');
                break;

            case "f": 
                f.play();
                $('#txtarea').val($('#txtarea').val() + 'F;');
                break;

            case "g": 
                g.play();
                $('#txtarea').val($('#txtarea').val() + 'G;');
                break;

            case "as": 
                as.play();
                $('#txtarea').val($('#txtarea').val() + 'A# B♭;');
                break;

            case "cs": 
                cs.play();
                $('#txtarea').val($('#txtarea').val() + 'C# D♭;');
                break;

            case "ds":
                ds.play();
                $('#txtarea').val($('#txtarea').val() + 'D# E♭;');
                break;

            case "fs": 
                fs.play();
                $('#txtarea').val($('#txtarea').val() + 'F# G♭;');
                break;

            case "gs": 
                gs.play();
                $('#txtarea').val($('#txtarea').val() + 'G# A♭;');
                break;
        }
    });

    $('#musicalnotesForm').submit(async function() {
        var notes = $('#txtarea').val();
        var result = notes.split(";");
        for (var i = 0; i < result.length - 1; i++) {
            switch (result[i]) {
                case "A": 
                    a.play();
                    break;
                case "B": 
                    b.play();
                    break;
                case "C": 
                    c.play();
                    break;
                case "D": 
                    d.play();
                    break;

                case "E": 
                    e.play();
                    break;

                case "F": 
                    f.play();
                    break;

                case "G": 
                    g.play();
                    break;

                case "A# B♭": 
                    as.play();
                    break;

                case "C# D♭": 
                    cs.play();
                    break;

                case "D# E♭":
                    ds.play();
                    break;

                case "F# G♭": 
                    fs.play();
                    break;

                case "G# A♭": 
                    gs.play();
                    break;

                default:
                    alert(result[i]);
            }
            await sleep(700)
        }
    });

    function sleep(ms) {
        //alert('chamou')
        return new Promise(resolve => setTimeout(resolve, ms));
    }


    var anchor = document.querySelector('#ancoraSalvar');
    anchor.onclick = function() {
        var container = document.querySelector('#txtarea');
        anchor.href = 'data:text/plain;charset=utf-8,' + encodeURIComponent(container.value);
        anchor.download = 'cifras.txt';
    };

});


